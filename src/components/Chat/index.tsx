import React, { useEffect, useState } from "react";
import { toJS } from "mobx";
import "./Chat.css";
import { useStore } from "../../state/store";
import { Messages } from "../Messages/Messages";
import { Inputs } from "../Input/Input";
import type { FCWithoutChildren } from "../../types/component";
import { observer } from "mobx-react";

const Chat: FCWithoutChildren = () => {
  const store = useStore();
  const [message, setMessage] = useState("");
  let messages = store.data.messages;
  const sendMessage = (event: any) => {
    event.preventDefault();
    if (message) {
      store.sendMessage(message);
    }
    setMessage("");
  };
  return (
    <div className="container">
      <Messages messages={messages} />
      <Inputs
        message={message}
        setMessage={setMessage}
        sendMessage={sendMessage}
      />
    </div>
  );
};

export default observer(Chat);
